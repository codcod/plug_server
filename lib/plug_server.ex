defmodule PlugServer do
  @moduledoc """
  Documentation for `PlugServer`.
  """
  import Plug.Conn

  def init(options) do
    IO.puts("here: server initialized")
    options
  end

  def call(conn, _opts) do
    #IO.puts("here: call")
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, "{\"Hello\": \"world\"}")
  end


end
