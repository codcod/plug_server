defmodule PlugServer.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: PlugServer.Worker.start_link(arg)
      # {PlugServer.Worker, arg}
      {Plug.Cowboy, scheme: :http, plug: PlugServer, options: [port: 8080]},
    ]

    opts = [strategy: :one_for_one, name: PlugServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
